const express = require('express');
const request = require('request');
const app = express();

app.listen(8080);

app.get('/', (req, res) => {
	let city = req.query.city;
	const request = require('request');
	request(
		`https://samples.openweathermap.org/data/2.5/forecast?q=${city}&appid=266103c3b385038d80a5c5cfe79034ff`,
		//const tempc = {data.list[0].main[0].temp};
		function(error, response, body) {
			let data = JSON.parse(body);
			if (response.statusCode === 200) {
				res.send(`The temperature of "${city}" is ${data.list[0].main[0].temp}`);
			}
		}
	);
});

app.listen(8080, () => console.log('Server running on port 8080'));